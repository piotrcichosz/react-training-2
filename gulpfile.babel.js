"use strict";

// Importowanie bibliotek które ogarniają wszystko :).
import gulp from 'gulp';
import babelify from 'babelify';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import browserSync from 'browser-sync';
import stylus from 'gulp-stylus';
import eslint from 'gulp-eslint';
import uglify from 'gulp-uglify';

let debug = true;
let bs = browserSync.create();

// # 1
// Stworzenie serwera, który będzie serwował pliki z katalogu ./dist
gulp.task('browser-sync', () => {
	bs.init({
		server: {
			baseDir: './dist'
		}
	});
});

// # 2
// Budowanie pliku bundle.js
gulp.task('build', () => {
	// Ustawienie trybu node na produkcję.
	// Dzięki temu przy minifikowaniu biblioteki są jeszcze mniejsze.
	// Bez tego w consoli będą błędy, że biblioteki reacta nie zostało zminifikowane.
	process.env.NODE_ENV = 'production';

	let stream = browserify({
			entries: './src/App.js', // Ściezka do pliku wejściowego, który będzie mielony.
			debug
		});

	return stream
		.transform('babelify') // Opcjonalnie można podać presety: {presets: ['es2015', 'react']}). Domyślnie pobierane są one z pliku .babelrc
		.bundle()
		.pipe(source('bundle.js')) // Nazwa pliku który zostanie utworzony.
		.pipe(buffer()) // Przekształceniu źródła pliku do gulp pipe
		.pipe(uglify({ // Minifikowanie pliku.
			mangle: false
		}))
		.pipe(gulp.dest('dist')) // Nazwa katalogu do którego będą wrzucone pliki.
		.pipe(bs.stream()); // Odświeżenie przeglądarki.
});

// # 3
// Sprawdzenie popraności kodu naszej appki.
gulp.task('lint-app', () => {
	return gulp.src(['./src/**/*.js','!node_modules/**'])
		.pipe(eslint())
		.pipe(eslint.format()) 
		.pipe(eslint.failAfterError());
});

// # 4
// Nasłuchowanie plików JS i CSS.
gulp.task('watch', ['build'], () => {
	gulp.watch('./src/**/*.js', ['build']); // Gdy jakiś plik z katalogu /src się zmieni, odpal taska "build".
	gulp.watch('./src/stylesheets/main.styl', ['css']); // Gdy zmieni się plik main.styl, odpal task "css".
});

// # 5
// Kompilowanie plików .styl do css.
gulp.task('css', function () {
  return gulp.src('./src/stylesheets/main.styl')
		.pipe(
			stylus({
				compress: true
			})
		)
		.pipe(gulp.dest('dist'))
		.pipe(bs.stream());
});

// # 6
// Kompilowanie pliku do zabaw.
// Zakomentowany task "lint-playground", który musi się wykonać przed odpaleniem.
gulp.task('playground', /*['lint-playground'],*/ () => {
	return browserify({
			entries: './src/playground.js',
			debug
		})
		.transform('babelify')
		.bundle()
		.pipe(source('playground.js'))
		.pipe(gulp.dest('dist-playground'));
});

// # 7
// Sprawdzenie popraności kodu pliku do zabaw.
gulp.task('lint-playground', () => {
	return gulp.src(['./src/playground.js'])
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});

// Definicja domyślnej komendy dla polecenia gulp.
// Odpalenie przeglądarki, hot-reload i nasłuchiwanie plików.
gulp.task('default', ['browser-sync', 'watch']);

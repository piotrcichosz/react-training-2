'use strict';

let numbers = [1,2,3,4,5,6,7,8,9,10];
numbers.filter((num) => num % 2 === 0);

const CONST_VAR = 1;

class Foo {
	param = {
		foo: 1,
		bar: 2
	}

	someMethod() {
		let something = 1;

		this.param.zzz = something;
	}
}

// CONST_VAR = 2;

let fooObj = new Foo();

console.log(CONST_VAR);

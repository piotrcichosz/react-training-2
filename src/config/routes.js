import MainPage from '../pages/MainPage';
import AboutPage from '../pages/AboutPage';
import HomePage from '../pages/HomePage';
import ToDoPage from '../pages/ToDoPage';

export default {
  path: '/',
  component: MainPage,
  indexRoute: { component: HomePage },
  childRoutes: [
    { path: 'about', component: AboutPage },
    { path: 'todo', component: ToDoPage }
  ]
};
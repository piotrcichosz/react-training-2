import alt from '../config/alt';

class ToDoAction {
	addItem(item) {
		return item;
	}

	markDone(id) {
		return id;
	}
}

export default alt.createActions(ToDoAction);
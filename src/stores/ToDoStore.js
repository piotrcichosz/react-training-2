import alt from '../config/alt';
import ToDoAction from '../actions/ToDoAction';
import ToDoListModel from '../models/ToDoListModel';

class ToDoStore {
	constructor() {
		this.bindListeners({
      addItem: ToDoAction.ADD_ITEM,
      markDone: ToDoAction.MARK_DONE
    });

		this.todos = new ToDoListModel();
	}

	addItem(item) {
		this.todos.addItem(item);
	}

	markDone(id) {
		this.todos.getItem(id).setDone(true);
	}
}

export default alt.createStore(ToDoStore, 'ToDoStore');
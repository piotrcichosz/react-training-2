import React from 'react';

export default class ToDoItem extends React.Component {
	onDone() {
		this.props.onDoneItem(this.props.index);
	}

	renderDoneLabel() {
		let todo = this.props.todo;
		return <strike>{todo.getTxt()}</strike>;
	}

	render() {
		let todo = this.props.todo;
		let id = `todo_${this.props.index}`;

		return (
			<li key={id} className={`todo-item ${todo.isDone() ? 'is-done' : ''}`}>
				<input type="checkbox" id={id} onClick={::this.onDone} />
				<label htmlFor={id}>
					{todo.isDone() ? this.renderDoneLabel() : todo.getTxt()}
				</label>
			</li>
		);
	}
}
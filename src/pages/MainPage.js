import React from 'react';
import MenuLink from '../components/MenuLink';

export default class MainPage extends React.Component {
	render() {
		return (
			<div>
				<ul>
					<li><MenuLink to="/">Główna</MenuLink></li>
					<li><MenuLink to="about">O projekcie</MenuLink></li>
					<li><MenuLink to="todo">Moje ToDo!</MenuLink></li>
				</ul>
				<hr />
				{this.props.children}
			</div>
		);
	}
}
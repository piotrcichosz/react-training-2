import React from 'react';
import ToDoItem from '../components/ToDoItem';
import ToDoItemModel from '../models/ToDoItemModel';

export default class ToDoPage extends React.Component {

	_items = [];

	state = {
		items: []
	}

	onNewItem(keyCode) {
		// ...
	}

	onDoneItem(id) {
		// ...
	}

	onKeyUp(e) {
		this.onNewItem(e.keyCode);
	}

	renderInput() {
		return (
			<div className="todo-input">
				<input
					type="text"
					ref={(input) => { this.input = input; }}
					onKeyUp={::this.onKeyUp}
					placeholder="Enter new ToDo item" />
			</div>
		);
	}

	showDone() {
		// ...
	}

	showAll() {
		// ...
	}

	renderItems() {
		return (
			<p>Lista itemów</p>
		);
	}

	render() {
		return (
			<div className="todo">
				<h1>Moja lista ToDo!!</h1>
				<a onClick={::this.showAll}>Pokaż wszystkie</a> | <a onClick={::this.showDone}>Pokaż ukończone</a>
				{this.renderInput()}
				{this.renderItems()}
			</div>
		);
	}
}

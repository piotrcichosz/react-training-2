import ToDoItemModel from './ToDoItemModel';

export default class ToDoListModel {
	model = {
		items: []
	};

	addItem(item) {
		this.model.items.push(new ToDoItemModel(item));
	}

	getItem(id) {
		return this.model.items[id];
	}

	getItems() {
		return this.model.items;
	}
}
export default class ToDoItemModel {
	model = {
		txt: '',
		done: false
	};

	constructor(txt) {
		this.model.txt = txt;
	}

	getTxt() {
		return this.model.txt;
	}

	setDone(bool) {
		this.model.done = !!bool;
	}

	isDone() {
		return this.model.done;
	}
}
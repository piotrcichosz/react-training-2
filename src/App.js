import React from 'react';
import ReactDOM from 'react-dom';
import { Router, hashHistory } from 'react-router';
import routes from './config/routes';
import alt from './config/alt';

let createElement = (Component, props) => {
	props['alt'] = alt;
	return <Component {...props} />
};

// https://github.com/ReactTraining/react-router/blob/master/docs/API.md#router

ReactDOM.render(
	<Router
		history={hashHistory}
		routes={routes}
		createElement={createElement} />,
	document.getElementById('app'));